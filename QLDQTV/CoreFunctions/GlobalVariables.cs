﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLDQTV.CoreFunctions
{
    public static class GlobalVariables
    {
        public static List<ObjectClasses.DQTV> dqtvList = new List<ObjectClasses.DQTV>();
        public const string ABOUT = "Phần mềm Quản lý Dân quan tự vệ đơn giản\n - Tác giả: Nguyễn Lý Quan \n - Email: quannnl92@gmail.com \n - Giấy phép: GNU General Public License";
    }
}
