﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace QLDQTV.ObjectClasses
{
    [Serializable]
    public class DQTV
    {
        private string id;
        private string ten;
        private string cccd;
        private string gioiTinh;
        [OptionalField]
        private DateTime ngaySinh;
        private string doiTuong;
        private string sdt;
        private string noiSinh;
        private string nguyenQuan;
        private string danToc;
        private string tonGiao;
        private string soNha1;
        private string tenDuong1;
        private string quanHuyen1;
        private string tinhThanhPho1;
        private string soNha2;
        private string tenDuong2;
        private string quanHuyen2;
        private string tinhThanhPho2;
        private string soNha3;
        private string tenDuong3;
        private string quanHuyen3;
        private string tinhThanhPho3;
        private DateTime ngayCapCccd;
        private string noiCapCccd;
        private string tinhTrangHonNhan;
        private string sucKhoe;
        private string thanhPhanBanThan;
        private string thanhPhanGiaDinh;
        private string canNang;
        private string vanHoa;
        private string trinhDo;
        private string chuyenNghanh;
        private string ngoaiNgu;
        private int namTotNghiep;
        private string ngheNghiep;
        private string noiLamViecHocTap;
        private string soNha4;
        private string tenDuong4;
        private string quanHuyen4;
        private string tinhThanhPho4;
        private string ngayVaoDang;
        private DateTime ngayChinhThucVaoDang;
        private DateTime ngayVaoDoan;
        private string tenCha;
        private int namSinhCha;
        private string ngheNghiepCha;
        private string tenMe;
        private int namSinhMe;
        private string ngheNghiepMe;
        private string quaTrinh6_18;
        private string quaTrinh18Nay;
        private string danhSach;
        private string nhom;

        public string ID { get { return id; } private set { id = value; } }
        public DateTime NgaySinh { get => ngaySinh; set => ngaySinh = value; }
        public string DoiTuong { get => doiTuong; set => doiTuong = value; }
        public string Sdt { get => sdt; set => sdt = value; }
        public string GioiTinh { get => gioiTinh; set => gioiTinh = value; }
        public string NoiSinh { get => noiSinh; set => noiSinh = value; }
        public string NguyenQuan { get => nguyenQuan; set => nguyenQuan = value; }
        public string DanToc { get => danToc; set => danToc = value; }
        public string TonGiao { get => tonGiao; set => tonGiao = value; }
        public string SoNha1 { get => soNha1; set => soNha1 = value; }
        public string TenDuong1 { get => tenDuong1; set => tenDuong1 = value; }
        public string QuanHuyen1 { get => quanHuyen1; set => quanHuyen1 = value; }
        public string TinhThanhPho1 { get => tinhThanhPho1; set => tinhThanhPho1 = value; }
        public string SoNha2 { get => soNha2; set => soNha2 = value; }
        public string TenDuong2 { get => tenDuong2; set => tenDuong2 = value; }
        public string QuanHuyen2 { get => quanHuyen2; set => quanHuyen2 = value; }
        public string TinhThanhPho2 { get => tinhThanhPho2; set => tinhThanhPho2 = value; }
        public string SoNha3 { get => soNha3; set => soNha3 = value; }
        public string TenDuong3 { get => tenDuong3; set => tenDuong3 = value; }
        public string QuanHuyen3 { get => quanHuyen3; set => quanHuyen3 = value; }
        public string TinhThanhPho3 { get => tinhThanhPho3; set => tinhThanhPho3 = value; }
        public string Cccd { get => cccd; set => cccd = value; }
        public DateTime NgayCapCccd { get => ngayCapCccd; set => ngayCapCccd = value; }
        public string NoiCapCccd { get => noiCapCccd; set => noiCapCccd = value; }
        public string TinhTrangHonNhan { get => tinhTrangHonNhan; set => tinhTrangHonNhan = value; }
        public string SucKhoe { get => sucKhoe; set => sucKhoe = value; }
        public string ThanhPhanBanThan { get => thanhPhanBanThan; set => thanhPhanBanThan = value; }
        public string ThanhPhanGiaDinh { get => thanhPhanGiaDinh; set => thanhPhanGiaDinh = value; }
        public string CanNang { get => canNang; set => canNang = value; }
        public string VanHoa { get => vanHoa; set => vanHoa = value; }
        public string TrinhDo { get => trinhDo; set => trinhDo = value; }
        public string ChuyenNghanh { get => chuyenNghanh; set => chuyenNghanh = value; }
        public string NgoaiNgu { get => ngoaiNgu; set => ngoaiNgu = value; }
        public int NamTotNghiep { get => namTotNghiep; set => namTotNghiep = value; }
        public string NgheNghiep { get => ngheNghiep; set => ngheNghiep = value; }
        public string NoiLamViecHocTap { get => noiLamViecHocTap; set => noiLamViecHocTap = value; }
        public string SoNha4 { get => soNha4; set => soNha4 = value; }
        public string TenDuong4 { get => tenDuong4; set => tenDuong4 = value; }
        public string QuanHuyen4 { get => quanHuyen4; set => quanHuyen4 = value; }
        public string TinhThanhPho4 { get => tinhThanhPho4; set => tinhThanhPho4 = value; }
        public string NgayVaoDang { get => ngayVaoDang; set => ngayVaoDang = value; }
        public DateTime NgayChinhThucVaoDang { get => ngayChinhThucVaoDang; set => ngayChinhThucVaoDang = value; }
        public DateTime NgayVaoDoan { get => ngayVaoDoan; set => ngayVaoDoan = value; }
        public string TenCha { get => tenCha; set => tenCha = value; }
        public int NamSinhCha { get => namSinhCha; set => namSinhCha = value; }
        public string NgheNghiepCha { get => ngheNghiepCha; set => ngheNghiepCha = value; }
        public string TenMe { get => tenMe; set => tenMe = value; }
        public int NamSinhMe { get => namSinhMe; set => namSinhMe = value; }
        public string NgheNghiepMe { get => ngheNghiepMe; set => ngheNghiepMe = value; }
        public string QuaTrinh6_18 { get => quaTrinh6_18; set => quaTrinh6_18 = value; }
        public string QuaTrinh18Nay { get => quaTrinh18Nay; set => quaTrinh18Nay = value; }
        public string DanhSach { get => danhSach; set => danhSach = value; }
        public string Nhom { get => nhom; set => nhom = value; }
        public string Ten { get => ten; set => ten = value; }

        private string generateID ()
        {
            //Using datetime for generating the unique ID for this object
            return DateTime.Now.ToString("yyyyMMddssff");
        }

        public DQTV ()
        {
            //Always generate new ID on new object
            this.ID = this.generateID();
        }
        public DQTV(string _Ten, string _Cccd)
        {
            //Always generate new ID on new object
            this.ID = this.generateID();
            this.Ten = _Ten;
            this.Cccd = _Cccd;
        }

        public DQTV (string _Ten, string _NgaySinh, string _GioiTinh, string _Cccd)
        {
            //Always generate new ID on new object
            this.ID = this.generateID();
            this.Ten = _Ten;
            this.NgaySinh = DateTime.Parse(_NgaySinh);
            this.GioiTinh = _GioiTinh;
            this.Cccd = _Cccd;
        }

        public bool saveFile(string _path)
        {
            string filename = _path + this.ID;
            if (Directory.Exists(_path) == false)
            {
                try
                {
                    Directory.CreateDirectory(_path);
                }
                catch (Exception error)
                {
                    Console.WriteLine(error);
                }
            }
            try
            {
                using (FileStream stream = new FileStream(@filename, FileMode.Create))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);
                }
            }
            catch(Exception error)
            {
                Console.WriteLine(error);
                return false;
            }
            return true;
        }

        public static DQTV loadFile(string _path,string _id)
        {
            string filename = _path + _id;
            try
            {
                using (FileStream stream = new FileStream(@filename, FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    return (DQTV)formatter.Deserialize(stream);
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error);
                return null;
            }
        }
    }
}