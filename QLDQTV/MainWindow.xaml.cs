﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QLDQTV.CoreFunctions;

namespace QLDQTV
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.Title = "Quản lý Dân quân tự vệ";

            ObjectClasses.DQTV test = new ObjectClasses.DQTV("20231020132022","Nguyễn Lý Quan","1997-02-06","Nam","025586737");
            GlobalVariables.dqtvList.Add(test);

            this.dgDQTV.ItemsSource = CoreFunctions.GlobalVariables.dqtvList;
        }

        public void refreshDGDQTV()
        {
            this.dgDQTV.Items.Refresh();
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(GlobalVariables.ABOUT,"Thông tin",MessageBoxButton.OK,MessageBoxImage.Information);
        }

        private void AddDQTV_Click(object sender, RoutedEventArgs e)
        {
            Window addDQTV = new ManageDVTV();
            addDQTV.Title = "Thêm mới Dân quân tự vệ";
            addDQTV.Show();
        }

        private void btFind_Click(object sender, RoutedEventArgs e)
        {
            ObjectClasses.DQTV test = new ObjectClasses.DQTV(DateTime.Now.ToString("yyyyMMddHHmmssff"), "Nguyễn Lý Quan", "1997-02-06", "Nam", "025586737");
            GlobalVariables.dqtvList.Add(test);
            refreshDGDQTV(); ;
        }

        private void copyDQTV ()
        {

        }

        private void dgDQTV_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.dgDQTV.SelectedItems.Count == 1)
            {
                this.btCopyDQTV.IsEnabled = true;
            }
            else
            {
                this.btCopyDQTV.IsEnabled = false;
            }
        }
    }
}
